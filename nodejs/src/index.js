const express = require('express');
const app = express();
const user = require('./users.json')

const Sequelize = require('sequelize');

const sequelize = new Sequelize('mysql://root@localhost:3306/tutor4dev')

app.use(express.urlencoded({ extended: true }))
sequelize.authenticate()
    .then(() => {
        console.log("connect");
    })

const fields = {
    id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
        defaultValue: 0
    },
    firstName: Sequelize.STRING,
    lastName: Sequelize.STRING

}
const options = {
    timestamps: false
}

const Customer = sequelize.define('customer', fields, options)

app.get('/customers', async (req, res) => { // get all and sort 
    try {
        const customers = await Customer.findAll({
            order: ['id']
        });
        res.json(customers)
        // res.json({})

    }

    catch (err) {
        res.json({ message: error.message }).status(500)
    }




});

app.get('/customer/:id', async (req, res) => {
    const id = req.params.id;

    try {
        const customer = await Customer.findOne({
            where: {
                id: id
            }
        })

        res.json(customer)
    } catch (err) {
        res.json({ message: error.message }).status(500)
    }
})

app.put('/customer/:id', async (req, res) => {
    const id = req.prarms.id;
    const body = req.body
    try {
        const customer = await Customer.findOne({
            where: {
                id: id
            }
        })

        if (customer) {
            await Customer.update(body)
            res.json(customer)
        } else {
            res.status(404).end()
        } y
    } catch (err) {
        res.json({ message: error.message }).status(500)
    }
})

app.post('/customers', async (req, res) => {
    const body = req.body;
    try {
        const customer = await Customer.create(body);
        res.json(customer)
    } catch (err) {

        res.json({ message: error.message }).status(500)

    }
})

app.delete('/customer/:id', async (req, res) => {
    const id = req.params.id;

    try {
        const customer = await Customer.findOne({
            where: {
                id: id
            }
        })

        if (customer) {
            await Customer.destory({
                where: {
                    id: id
                }
            })
            res.json(customer)
        } else {
            res.status(404).end()
        }
    } catch (err) {
        res.json({ message: error.message }).status(500)
    }
})


app.listen(3000, () => {
    console.log('done')
});
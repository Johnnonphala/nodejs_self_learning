const express = require('express');
const app = express();
const user = require('./users.json')
// ใช้ตัวแปร req เมื่อสนใจสิ่งที่วื่งเข้ามา 

app.get('/', (req, res) => {
    res.send('hello');
})

app.get('/users', (req, res) => {  // get all
    res.json(user)
})


app.get('/user/:id/', (req, res) => {
    const id = req.params.id;
    const users = user.find((each) => {
        console.log(each)
        return each.id == id;
    })
    res.send(users)
})

app.use(express.urlencoded({ extended: true }))
app.post('/users', (req, res) => {
    const body = req.body
    console.log(body)
    res.json(body)
})
